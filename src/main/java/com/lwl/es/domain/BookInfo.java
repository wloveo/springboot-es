package com.lwl.es.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
public class BookInfo {

    private String title;

    private String author;

    private String content;
    /**
     *  出版日期
     */
    private Date publishTime;

}
