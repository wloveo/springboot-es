package com.lwl.es.bulk;

import com.lwl.es.BaseApiTest;
import org.elasticsearch.action.get.MultiGetItemResponse;
import org.elasticsearch.action.get.MultiGetRequest;
import org.elasticsearch.action.get.MultiGetResponse;
import org.elasticsearch.client.RequestOptions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;

@SpringBootTest
public class MultiGetApiTest extends BaseApiTest {

    @Test
    public void multi() throws IOException {

        // 批量获取
        MultiGetRequest request = new MultiGetRequest();

        request.add(new MultiGetRequest.Item(INDEX_NAME,"11"))
                .add(new MultiGetRequest.Item(INDEX_NAME,"12"))
                .add(new MultiGetRequest.Item(INDEX_NAME,"13"))
                .add(new MultiGetRequest.Item(INDEX_NAME,"14"));

        // 同时也可以设置和 GetRequest 同样的参数，比如 只去部分字段，或者排除某些字段

        MultiGetResponse mget = restHighLevelClient.mget(request, RequestOptions.DEFAULT);
        Arrays.stream(mget.getResponses()).forEach(r-> System.out.println(r.getResponse()));

        /**
         * {"_index":"book_vs","_type":"_doc","_id":"11","_version":1,"_seq_no":18,"_primary_term":2,"found":true,"_source":{"publishTime":1616044693280,"author":"流星11","title":"elasticsearch初学者11","content":"我是书籍内容11"}}
         * {"_index":"book_vs","_type":"_doc","_id":"12","_version":1,"_seq_no":19,"_primary_term":2,"found":true,"_source":{"publishTime":1616044693281,"author":"流星12","title":"elasticsearch初学者12","content":"我是书籍内容12"}}
         * {"_index":"book_vs","_type":"_doc","_id":"13","_version":1,"_seq_no":20,"_primary_term":2,"found":true,"_source":{"publishTime":1616044693281,"author":"流星13","title":"elasticsearch初学者13","content":"我是书籍内容13"}}
         * {"_index":"book_vs","_type":"_doc","_id":"14","_version":1,"_seq_no":21,"_primary_term":2,"found":true,"_source":{"publishTime":1616044693281,"author":"流星14","title":"elasticsearch初学者14","content":"我是书籍内容14"}}
         */

    }

}
