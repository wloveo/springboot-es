package com.lwl.es.bulk;

import com.alibaba.fastjson.JSON;
import com.lwl.es.BaseApiTest;
import com.lwl.es.domain.BookInfo;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class BulkRequestApiTest extends BaseApiTest {


    @Test
    public void bulk() throws IOException {
        BulkRequest request = new BulkRequest();

        // 批量新增书籍
        for (int i=10;i<20;i++) {
            BookInfo info = new BookInfo().setAuthor("流星"+i)
                    .setPublishTime(new Date())
                    .setTitle("elasticsearch初学者"+i).setContent("我是书籍内容" + i);
            IndexRequest index = new IndexRequest(INDEX_NAME);
            index.id(""+i).source(JSON.toJSON(info).toString(), XContentType.JSON);
            request.add(index);
        }

        BulkResponse bulk = restHighLevelClient.bulk(request, RequestOptions.DEFAULT);
        System.out.println(bulk);
        // 获取一个数据看看
        getData("15");
        /**
         * org.elasticsearch.action.bulk.BulkResponse@36d33f4
         * 获取的数据:
         * {publishTime=1616044693281, author=流星15, title=elasticsearch初学者15, content=我是书籍内容15}
         * {"_index":"book_vs","_type":"_doc","_id":"15","_version":1,"_seq_no":22,"_primary_term":2,"found":true,"_source":{"publishTime":1616044693281,"author":"流星15","title":"elasticsearch初学者15","content":"我是书籍内容15"}}
         */
    }

    /**
     *  针对不同的类型操作
     * result:
     * author: lwl
     * date: 2021/3/18 13:18
     */
    @Test
    public void bulkDiff() throws IOException {
        BulkRequest request = new BulkRequest();

        // 新增一条
        BookInfo info = new BookInfo().setAuthor("流星9")
                .setPublishTime(new Date())
                .setTitle("elasticsearch初学者9").setContent("我是书籍内容9");
        IndexRequest index = new IndexRequest(INDEX_NAME);
        index.id("9").source(JSON.toJSON(info).toString(), XContentType.JSON);
        request.add(index);
        // 修改一条
        UpdateRequest upd = new UpdateRequest(INDEX_NAME,"15");
        Map<String,Object> param = new HashMap<>();
        param.put("author","jack");
        param.put("newField","我是新增的字段");
        upd.doc(param);
        request.add(upd);

        // 删除一条
        DeleteRequest del = new DeleteRequest(INDEX_NAME,"19");
        request.add(del);


        // 执行
        BulkResponse bulk = restHighLevelClient.bulk(request, RequestOptions.DEFAULT);

        // 针对不同的操作，返回的结果类型也应该是不同的
        Arrays.stream(bulk.getItems()).forEach(r->{
            DocWriteResponse response = r.getResponse();
            switch (r.getOpType()) {
                case INDEX:
                case CREATE:
                    IndexResponse indexResponse = (IndexResponse) response;
                    // 新增一个索引信息
                    System.out.println("新增一个索引信息 "+response.getId()+" 处理状态："+ response.status());
                    break;
                case UPDATE:
                    UpdateResponse updateResponse = (UpdateResponse) response;
                    // 更新一个操作响应
                    System.out.println("更新一个索引信息 "+response.getId()+" 处理状态："+ response.status());
                    break;
                case DELETE:
                    DeleteResponse deleteResponse = (DeleteResponse) response;
                    // 更新一个操作响应
                    System.out.println("删除一个索引信息 "+response.getId()+" 处理状态："+ response.status());
            }
        });

        // 查询新增的数据
        getData("9");
        // 查询修改的数据
        getData("15");
        // 查询删除的数据
        getData("19");

        /**
         * 新增一个索引信息 9 处理状态：CREATED
         * 更新一个索引信息 15 处理状态：OK
         * 删除一个索引信息 19 处理状态：NOT_FOUND
         * 获取的数据:
         * {publishTime=1616045393384, author=流星9, title=elasticsearch初学者9, content=我是书籍内容9}
         * {"_index":"book_vs","_type":"_doc","_id":"9","_version":1,"_seq_no":27,"_primary_term":2,"found":true,"_source":{"publishTime":1616045393384,"author":"流星9","title":"elasticsearch初学者9","content":"我是书籍内容9"}}
         * 获取的数据:
         * {publishTime=1616044693281, author=jack, newField=我是新增的字段, title=elasticsearch初学者15, content=我是书籍内容15}
         * {"_index":"book_vs","_type":"_doc","_id":"15","_version":2,"_seq_no":28,"_primary_term":2,"found":true,"_source":{"publishTime":1616044693281,"author":"jack","title":"elasticsearch初学者15","content":"我是书籍内容15","newField":"我是新增的字段"}}
         * 获取的数据:
         * null
         * {"_index":"book_vs","_type":"_doc","_id":"19","found":false}
         */
    }


}
