package com.lwl.es;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class LwlEsApplicationTests {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    // https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.4/java-rest-high-supported-apis.html

    @Test
    public void getEsInfo() throws IOException {

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // SearchRequest
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.source(searchSourceBuilder);
        // 查询ES
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        System.out.println(searchResponse);
    }

    @Test
    public void get() throws IOException {
        // GetRequest
        GetRequest getRequest = new GetRequest("book_vs", "1");
        // 查询ES
        GetResponse getResponse = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);

        System.out.println(getResponse);
        System.out.println(getResponse.getSourceAsString());
        //{"_index":"book_vs","_type":"_doc","_id":"1","_version":1,"_seq_no":0,"_primary_term":1,"found":true,"_source":{"publishTime":1615888630774,"author":"流星","title":"elasticsearch初学者","content":"如果是TransportClient，可以参考这篇文章。\n\n如果使用的是RestClient，可以参考这篇文章：https://www.jianshu.com/p/2ff05a83816e；这种方式也是官网推荐的；\n\n如果使用的是SpringData-Es，可以参考：https://blog.csdn.net/tianyaleixiaowu/article/details/76149547/ (介绍了ElasticsearchRepoistory和ElasticsearchTemplate两种方法的具体使用。)；"}}
        /**
         * {
         *     "_index": "book_vs",
         *     "_type": "_doc",
         *     "_id": "1",
         *     "_version": 1,
         *     "_seq_no": 0,
         *     "_primary_term": 1,
         *     "found": true,
         *     "_source": {
         *         "publishTime": 1615888630774,
         *         "author": "流星",
         *         "title": "elasticsearch初学者",
         *         "content": "如果是TransportClient，可以参考这篇文章。\n\n如果使用的是RestClient，可以参考这篇文章：https://www.jianshu.com/p/2ff05a83816e；这种方式也是官网推荐的；\n\n如果使用的是SpringData-Es，可以参考：https://blog.csdn.net/tianyaleixiaowu/article/details/76149547/ (介绍了ElasticsearchRepoistory和ElasticsearchTemplate两种方法的具体使用。)；"
         *     }
         * }
         */
        //{"publishTime":1615888630774,"author":"流星","title":"elasticsearch初学者","content":"如果是TransportClient，可以参考这篇文章。\n\n如果使用的是RestClient，可以参考这篇文章：https://www.jianshu.com/p/2ff05a83816e；这种方式也是官网推荐的；\n\n如果使用的是SpringData-Es，可以参考：https://blog.csdn.net/tianyaleixiaowu/article/details/76149547/ (介绍了ElasticsearchRepoistory和ElasticsearchTemplate两种方法的具体使用。)；"}
    }

}
