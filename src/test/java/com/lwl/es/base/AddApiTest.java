package com.lwl.es.base;

import com.alibaba.fastjson.JSONObject;
import com.lwl.es.domain.BookInfo;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Cancellable;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Date;

@SpringBootTest
public class AddApiTest {

    //https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-supported-apis.html
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test
    public void addIndex() throws IOException {

        String indexName = "book_vs";

        IndexRequest request = new IndexRequest(indexName);
        request.id("122");
        BookInfo info = new BookInfo().setAuthor("33流星").setPublishTime(new Date()).setTitle("elastic初学者").setContent("如果是TransportClient，可以参考这篇文章。\n" +
                "\n" +
                "如果使用的是RestClient，可以参考这篇文章：https://www.jianshu.com/p/2ff05a83816e；这种方式也是官网推荐的；\n" +
                "\n" +
                "如果使用的是SpringData-Es，可以参考：https://blog.csdn.net/tianyaleixiaowu/article/details/76149547/ (介绍了ElasticsearchRepoistory和ElasticsearchTemplate两种方法的具体使用。)；");
        request.source(JSONObject.toJSON(info).toString(), XContentType.JSON);
        IndexResponse index = restHighLevelClient.index(request, RequestOptions.DEFAULT);
        System.out.println(index);
        // IndexResponse[index=book_vs,type=_doc,id=1,version=1,result=created,seqNo=0,primaryTerm=1,shards={"total":2,"successful":1,"failed":0}]
    }


    /**
     *  异步添加数据
     * result:
     * author: lwl
     * date: 2021/3/17 9:42
     */
    @Test
    public void addAsync() throws Exception {

        String indexName = "book_vs";

        IndexRequest request = new IndexRequest(indexName);
        request.id("3");
        BookInfo info = new BookInfo().setAuthor("雷神")
                .setPublishTime(new Date()).setTitle("elasticsearch入门")
                .setContent("Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. " +
                        "Users need to specify how the response or potential failures " +
                        "will be handled by passing the request and a listener to the asynchronous index method");
        request.source(JSONObject.toJSON(info).toString(), XContentType.JSON);
        Cancellable index = restHighLevelClient.indexAsync(request, RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
                System.out.println("数据处理完成：");
                System.out.println(indexResponse);
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println("数据处理异常：");
                e.printStackTrace();
            }
        });
        System.out.println("异步写入ES: ");
        System.out.println(index);

        // 等待数据写入完成
        Thread.sleep(10_000L);
        /**
         * 异步写入ES:
         * org.elasticsearch.client.Cancellable@4b960b5b
         * 数据处理完成：
         * IndexResponse[index=book_vs,type=_doc,id=3,version=1,result=created,seqNo=1,primaryTerm=2,shards={"total":2,"successful":1,"failed":0}]
         */
    }

}
