package com.lwl.es.base;

import com.lwl.es.BaseApiTest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.client.RequestOptions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
public class DelApiTest extends BaseApiTest {


    @Test
    public void del() throws IOException {
        DeleteRequest request = new DeleteRequest(INDEX_NAME);
        // 删除一个不存在的
        request.id("2");
        DeleteResponse delete = restHighLevelClient.delete(request, RequestOptions.DEFAULT);

        System.out.println("删除不存在的数据：");
        System.out.println(delete);
        System.out.println();
        System.out.println();

        // 删除一个存在的
        request.id("1");
        delete = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
        System.out.println("删除存在的数据：");
        System.out.println(delete);
        System.out.println();
        System.out.println();

        // 再次删除同一个ID
        request.id("1");
        delete = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
        System.out.println("再次删除同一个ID的数据：");
        System.out.println(delete);
        System.out.println();
        System.out.println();

        /**
         *  result=not_found 为 表示未查询到
         */
        /**
         * 删除不存在的数据：
         * DeleteResponse[index=book_vs,type=_doc,id=2,version=1,result=not_found,shards=ShardInfo{total=2, successful=1, failures=[]}]
         *
         *
         * 删除存在的数据：
         * DeleteResponse[index=book_vs,type=_doc,id=1,version=2,result=deleted,shards=ShardInfo{total=2, successful=1, failures=[]}]
         *
         *
         * 再次删除同一个ID的数据：
         * DeleteResponse[index=book_vs,type=_doc,id=1,version=3,result=not_found,shards=ShardInfo{total=2, successful=1, failures=[]}]
         */
    }

}
