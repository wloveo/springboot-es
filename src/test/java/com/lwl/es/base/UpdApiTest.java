package com.lwl.es.base;

import com.lwl.es.BaseApiTest;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
public class UpdApiTest extends BaseApiTest {


    @Test
    public void upd() throws IOException {

        getData("3");


        // 更新一个数据
        UpdateRequest request = new UpdateRequest(INDEX_NAME,"3");

        // 通过脚本更新
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title","elasticsearch是有效啊结果哦");
//        parameters.put("newField","新增的属性");
        Script inline = new Script(ScriptType.INLINE, "painless",
                "ctx._source.title += params.title", parameters);

        request.script(inline);

        UpdateResponse update = restHighLevelClient.update(request, RequestOptions.DEFAULT);
        System.out.println("更新结果：");
        System.out.println(update);

        getData("3");

        /**
         * 通过这种方式，只能一次更改一个字段，而且是追加内容，而不是替换
         *   可以通过这种方式新增新的字段
         */
        /**
         * 获取的数据:
         * {"_index":"book_vs","_type":"_doc","_id":"3","_version":2,"_seq_no":5,"_primary_term":2,"found":true,"_source":{"publishTime":1615945655203,"author":"雷神","title":"elasticsearch入门","content":"Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method","field":"nullelasticsearch被更新的title"}}
         *
         * 更新结果：
         * UpdateResponse[index=book_vs,type=_doc,id=3,version=3,seqNo=6,primaryTerm=2,result=updated,shards=ShardInfo{total=2, successful=1, failures=[]}]
         * 获取的数据:
         * {"_index":"book_vs","_type":"_doc","_id":"3","_version":3,"_seq_no":6,"_primary_term":2,"found":true,"_source":{"publishTime":1615945655203,"author":"雷神","title":"elasticsearch入门elasticsearch被更新的title","content":"Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method","field":"nullelasticsearch被更新的title"}}
         */

    }

    @Test
    public void updByMap() throws IOException {

        getData("3");
        // 更新一个数据
        UpdateRequest request = new UpdateRequest(INDEX_NAME,"3");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title","elasticsearch被更新的title3333");
        parameters.put("newField","我是新增的字段");
        parameters.put("author","我是新作者3333");
        request.doc(parameters);
        UpdateResponse update = restHighLevelClient.update(request, RequestOptions.DEFAULT);
        System.out.println("更新结果：");
        System.out.println(update);

        getData("3");

        /**
         *  这种方式更加通俗方便理解，而且可以看出  更新的值是被替换的，也可以新增字段
         */
        /**
         * 获取的数据:
         * {publishTime=1615945655203, field=nullelasticsearch被更新的title, author=我是新作者, newField=我是新增的字段, title=elasticsearch被更新的title2222222, content=Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method}
         *
         * 更新结果：
         * UpdateResponse[index=book_vs,type=_doc,id=3,version=6,seqNo=9,primaryTerm=2,result=updated,shards=ShardInfo{total=2, successful=1, failures=[]}]
         * 获取的数据:
         * {publishTime=1615945655203, field=nullelasticsearch被更新的title, author=我是新作者3333, newField=我是新增的字段, title=elasticsearch被更新的title3333, content=Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method}
         */

    }


    @Test
    public void updNotIn() throws IOException {

        // 更新一个不存在的数据，则会抛出异常
        UpdateRequest request = new UpdateRequest(INDEX_NAME, "1");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", "elasticsearch被更新的title3333");
        parameters.put("newField", "我是新增的字段");
        parameters.put("author", "我是新作者3333");
        request.doc(parameters);
        try {
            UpdateResponse update = restHighLevelClient.update(request, RequestOptions.DEFAULT);
            System.out.println("更新一个不存在的结果：");
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                System.out.println(e);
                /**
                 * [book_vs/hZOURZJfR9e-_1kz-cqSOQ][[book_vs][0]]
                 *      ElasticsearchStatusException[Elasticsearch exception [type=document_missing_exception, reason=[_doc][1]: document missing]]
                 */
            }
        }

    }

    @Test
    public void updNotInButInsert() throws IOException {

        // 更新一个数据，如果不存在，则新增
        UpdateRequest request = new UpdateRequest(INDEX_NAME, "7");
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("title", "elasticsearch我是不存在更新的");
        parameters.put("content", "我是内容");
        parameters.put("author", "不存在的作者");
        parameters.put("publishTime", new Date().getTime());
        // 如果数据不存在，则新增
        /**
         *  这里如果直接使用 request.upsert(parameters) 则会报出异常：script or doc is missing  就是必须要有一个doc
         */
//        request.upsert(parameters);
        request.doc(parameters).docAsUpsert(true);
        try {
            UpdateResponse update = restHighLevelClient.update(request, RequestOptions.DEFAULT);
            System.out.println("更新一个不存在的结果：");
            System.out.println(update);
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                System.out.println("数据发生了异常："+e.toString());
            }else {
                e.printStackTrace();
            }
        }

        /**
         *  这里面 result=created 表示的是创建
         * 更新一个不存在的结果：
         * UpdateResponse[index=book_vs,type=_doc,id=1,version=1,seqNo=10,primaryTerm=2,result=created,shards=ShardInfo{total=2, successful=1, failures=[]}]
         */

    }

    @Test
    public void get() throws IOException {
        System.out.println("查看上一个更新插入的数据：");
        getData("7");
    }



}
