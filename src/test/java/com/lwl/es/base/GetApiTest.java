package com.lwl.es.base;

import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.Cancellable;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.Strings;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Map;

@SpringBootTest
public class GetApiTest {

    //https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-supported-apis.html
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    /**
     *  索引名称
     */
    private static final String INDEX_NAME = "book_vs";

    @Test
    public void getId() throws IOException {
        GetRequest request = new GetRequest();
        request.index(INDEX_NAME).id("2");
        GetResponse doc = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        // 输出结果集
        System.out.println(doc);
        /**
         *{
         *     "_index": "book_vs",
         *     "_type": "_doc",
         *     "_id": "3",
         *     "_version": 1,
         *     "_seq_no": 1,
         *     "_primary_term": 2,
         *     "found": true,
         *     "_source": {
         *         "publishTime": 1615945655203,
         *         "author": "雷神",
         *         "title": "elasticsearch入门",
         *         "content": "Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method"
         *     }
         * }
         */
    }

    /**
     *  设置返回结果包含的字段
     * result:
     * author: lwl
     * date: 2021/3/17 9:58
     */
    @Test
    public void getIdForIncludeField() throws IOException {
        GetRequest request = new GetRequest();
        request.index(INDEX_NAME).id("3");

        // 设置返回的字段即：_source里面的字段
        String[] includes = new String[]{"title", "author","publishTime"};
        String[] excludes = Strings.EMPTY_ARRAY;
        FetchSourceContext fetchSourceContext =
                new FetchSourceContext(true, includes, excludes);
        request.fetchSourceContext(fetchSourceContext);

        GetResponse doc = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        // 输出结果集
        System.out.println(doc);
        // 结果集中只有我们设定的几个字段
        /**
         * {
         *     "_index": "book_vs",
         *     "_type": "_doc",
         *     "_id": "3",
         *     "_version": 1,
         *     "_seq_no": 1,
         *     "_primary_term": 2,
         *     "found": true,
         *     "_source": {
         *         "publishTime": 1615945655203,
         *         "author": "雷神",
         *         "title": "elasticsearch入门"
         *     }
         * }
         */

        Map<String, Object> source = doc.getSource();
        System.out.println("输出返回的结果对象：source");
        System.out.println(source);
    }

    /**
     * 设置返回结果排除的字段
     *  异步返回结果
     * result:
     * author: lwl
     * date: 2021/3/17 9:58
     */
    @Test
    public void getIdForExcludeField() throws Exception {
        GetRequest request = new GetRequest();
        request.index(INDEX_NAME).id("3");

        // 设置返回的字段即：_source里面的字段
        String[] includes = Strings.EMPTY_ARRAY;
        String[] excludes = new String[]{"publishTime"};
        FetchSourceContext fetchSourceContext =
                new FetchSourceContext(true, includes, excludes);
        request.fetchSourceContext(fetchSourceContext);

        Cancellable async = restHighLevelClient.getAsync(request, RequestOptions.DEFAULT, new ActionListener<GetResponse>() {
            @Override
            public void onResponse(GetResponse documentFields) {
                System.out.println("异步完成结果回调：");
                System.out.println(documentFields);
                System.out.println(documentFields.getSourceAsString());
            }

            @Override
            public void onFailure(Exception e) {
                System.out.println("异步失败结果回调：");
                e.printStackTrace();
            }
        });

        System.out.println("异步返回主线程：");
        System.out.println(async.toString());

        // 主线程暂停5秒，等待结束
        Thread.sleep(5_000L);

        /**
         *
         * 异步返回主线程：
         * org.elasticsearch.client.Cancellable@56f730b2
         * 异步完成结果回调：
         * {
         *     "_index": "book_vs",
         *     "_type": "_doc",
         *     "_id": "3",
         *     "_version": 1,
         *     "_seq_no": 1,
         *     "_primary_term": 2,
         *     "found": true,
         *     "_source": {
         *         "author": "雷神",
         *         "title": "elasticsearch入门",
         *         "content": "Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method"
         *     }
         * }
         *
         * {
         *     "author": "雷神",
         *     "title": "elasticsearch入门",
         *     "content": "Executing a IndexRequest can also be done in an asynchronous fashion so that the client can return directly. Users need to specify how the response or potential failures will be handled by passing the request and a listener to the asynchronous index method"
         * }
         */
    }

    @Test
    public void getIdForStoredField() throws IOException {
        GetRequest request = new GetRequest();
        request.index(INDEX_NAME).id("3");
        // 当store为false时(默认配置），这些field只存储在"_source" field中。
        // 当store为true时，这些field的value会存储在一个跟 _source 平级的独立的field中。同时也会存储在_source中，所以有两份拷贝。

        // 换句话说，就是把某些字段设置store为true时，则该字段会在fields 和 _source中出现，
        // 如果是通过storedFields查询的话，则返回结果只会在fields中查询和返回，不关_source什么事，相反同理
        request.storedFields("title");
        GetResponse doc = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        // 输出结果集
        System.out.println("输出返回的结果对象：GetResponse");
        System.out.println(doc);

    }
}
