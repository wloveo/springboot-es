package com.lwl.es.base;

import com.lwl.es.BaseApiTest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.core.TermVectorsRequest;
import org.elasticsearch.client.core.TermVectorsResponse;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

/**
 * @description： 词向量
 * @author     ：lwl
 * @date       ：2021/3/18 9:59
 * @version:     1.0.0
 */
@SpringBootTest
public class TermVectorsApiTest extends BaseApiTest {


    @Test
    public void termVectorsTest() throws IOException {

        getData("3");

        TermVectorsRequest request = new TermVectorsRequest(INDEX_NAME,"3");
        request.setFields("title");


        TermVectorsResponse termvectors = restHighLevelClient.termvectors(request, RequestOptions.DEFAULT);

        System.out.println();
        termvectors.getTermVectorsList().stream().forEach(r-> {
            System.out.println(r.getFieldName());
            System.out.println(r.getFieldStatistics().getDocCount()+" " + r.getFieldStatistics().getSumDocFreq()
                    + " " + r.getFieldStatistics().getSumTotalTermFreq());

            r.getTerms().stream().forEach(j->{
                System.out.println("-------");
                System.out.println(j.getTerm());
            });
        });
    }

}
