package com.lwl.es.base;

import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
public class ExistsApiTest {

    //https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-supported-apis.html
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    /**
     *  索引名称
     */
    private static final String INDEX_NAME = "book_vs";

    /**
     *
     * 它像使用Get API一样使用GetRequest。它的所有可选参数都受到支持。由于exists()只返回true或false，
     *  我们建议关闭获取源和任何存储字段，这样请求就会稍微轻一些
     * result:
     * author: lwl
     * date: 2021/3/17 11:06
     */
    @Test
    public void exists() throws IOException {

        GetRequest request = new GetRequest(INDEX_NAME);
        request.id("1");
        // 设置_source 返回字段为空
        request.fetchSourceContext(new FetchSourceContext(false));
        // 设置fields 返回为空
        request.storedFields("_none_");

        boolean exists = restHighLevelClient.exists(request, RequestOptions.DEFAULT);
        System.out.println("是否存在：" + exists);
        /**
         * 是否存在：true
         */
    }

}
