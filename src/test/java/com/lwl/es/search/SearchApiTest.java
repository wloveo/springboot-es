package com.lwl.es.search;

import com.lwl.es.BaseApiTest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;

@SpringBootTest
public class SearchApiTest extends BaseApiTest {


    @Test
    public void search() throws IOException {

        SearchRequest searchRequest = new SearchRequest();
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        Arrays.stream(search.getHits().getHits()).forEach(r-> System.out.println(r.getIndex()+ " : "+ r.getSourceAsString()));

    }

    /**
     *
     * 在某个索引上检索
     * result:
     * author: lwl
     * date: 2021/3/19 10:21
     */
    @Test
    public void searchIndex() throws IOException {

        SearchRequest searchRequest = new SearchRequest("book_vs2");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        Arrays.stream(search.getHits().getHits()).forEach(r-> System.out.println(r.getIndex()+ " : "+ r.getSourceAsString()));

    }

    @Test
    public void searchPage() throws IOException {

        SearchRequest searchRequest = new SearchRequest(INDEX_NAME);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.from(0).size(5);
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        Arrays.stream(search.getHits().getHits()).forEach(r-> System.out.println(r.getIndex()+ " : "+ r.getSourceAsString()));

    }

    @Test
    public void searchPageOrder() throws IOException {

        SearchRequest searchRequest = new SearchRequest(INDEX_NAME);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        // 分页
        searchSourceBuilder.from(0).size(5);
        // 排序
        searchSourceBuilder.sort("publishTime", SortOrder.DESC);
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        Arrays.stream(search.getHits().getHits()).forEach(r-> System.out.println(r.getIndex()+ " : "+ r.getSourceAsString()));

    }

    @Test
    public void searchField() throws IOException {

        SearchRequest searchRequest = new SearchRequest(INDEX_NAME);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 某个字段模糊查询
        searchSourceBuilder.query(QueryBuilders.wildcardQuery("title", "*search"));
        // 大于等于 或者小于等于

        // 分页
        searchSourceBuilder.from(0).size(5);
        // 排序
        searchSourceBuilder.sort("publishTime", SortOrder.DESC);
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        System.out.println(search);
        Arrays.stream(search.getHits().getHits()).forEach(r-> System.out.println(r.getIndex()+ " : "+ r.getSourceAsString()));

    }

    @Test
    public void searchHighlight() throws IOException {

        SearchRequest searchRequest = new SearchRequest(INDEX_NAME);

        // 高亮
        HighlightBuilder highlightBuilder = new HighlightBuilder();

        HighlightBuilder.Field field = new HighlightBuilder.Field("title");
        field.highlighterType("unified");
        highlightBuilder.field(field);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 某个字段模糊查询
        searchSourceBuilder.highlighter(highlightBuilder);
        searchSourceBuilder.query(QueryBuilders.wildcardQuery("title", "*search"));
        // 大于等于 或者小于等于

        // 分页
        searchSourceBuilder.from(0).size(1);
        // 排序
        searchSourceBuilder.sort("publishTime", SortOrder.DESC);
        searchRequest.source(searchSourceBuilder);

        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);

        System.out.println(search);
        Arrays.stream(search.getHits().getHits()).forEach(r-> System.out.println(r.getIndex()+ " : "+ r.getSourceAsString()));

        /**
         * {
         *     "took": 3,
         *     "timed_out": false,
         *     "_shards": {
         *         "total": 1,
         *         "successful": 1,
         *         "skipped": 0,
         *         "failed": 0
         *     },
         *     "hits": {
         *         "total": {
         *             "value": 12,
         *             "relation": "eq"
         *         },
         *         "max_score": null,
         *         "hits": [
         *             {
         *                 "_index": "book_vs",
         *                 "_type": "_doc",
         *                 "_id": "9",
         *                 "_score": null,
         *                 "_source": {
         *                     "publishTime": 1616045798817,
         *                     "author": "流星9",
         *                     "title": "elasticsearch初学者9",
         *                     "content": "我是书籍内容9"
         *                 },
         *                 "highlight": {
         *                     "title": [
         *                         "<em>elasticsearch</em>初学者9"
         *                     ]
         *                 },
         *                 "sort": [
         *                     1616045798817
         *                 ]
         *             }
         *         ]
         *     }
         * }
         */
    }
}
