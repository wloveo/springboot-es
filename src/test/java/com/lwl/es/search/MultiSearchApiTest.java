package com.lwl.es.search;

import com.lwl.es.BaseApiTest;
import org.elasticsearch.action.search.MultiSearchRequest;
import org.elasticsearch.action.search.MultiSearchResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Arrays;

@SpringBootTest
public class MultiSearchApiTest extends BaseApiTest {

    @Test
    public void multi() throws IOException {

        MultiSearchRequest request = new MultiSearchRequest();

        // 构建第一个
        SearchRequest firstSearchRequest = new SearchRequest(INDEX_NAME);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("title","作者"));
        firstSearchRequest.source(searchSourceBuilder);
        request.add(firstSearchRequest);


        SearchRequest secondSearchRequest = new SearchRequest("book_vs2");
        searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchQuery("author","2"));
        secondSearchRequest.source(searchSourceBuilder);
        request.add(secondSearchRequest);

        MultiSearchResponse msearch = restHighLevelClient.msearch(request, RequestOptions.DEFAULT);

        Arrays.stream(msearch.getResponses()).forEach(r-> System.out.println(r.getResponse()));


    }

}
