package com.lwl.es;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
public class BaseApiTest {

    //https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.7/java-rest-high-supported-apis.html
    @Autowired
    public RestHighLevelClient restHighLevelClient;

    /**
     *  索引名称
     */
    public static final String INDEX_NAME = "book_vs";

    public void getData(String id) throws IOException {
        GetRequest getRequest = new GetRequest();
        getRequest.index(INDEX_NAME).id(id);
        GetResponse doc = restHighLevelClient.get(getRequest, RequestOptions.DEFAULT);
        // 输出结果集
        System.out.println("获取的数据: ");
        System.out.println(doc.getSource());
        System.out.println(doc);
    }

    public void delData(String id) throws IOException {
        DeleteRequest request = new DeleteRequest();
        request.index(INDEX_NAME).id(id);
         DeleteResponse doc = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
        // 输出结果集
        System.out.println("删除的数据: ");
        System.out.println(doc);
    }
}
